document.addEventListener('DOMContentLoaded', function () {
  var splide = new Splide(".splide", {
    type: 'loop',

    autoplay: true,
    interval: 6000,
    pauseOnHover: true,

    cover: true,
    autoHeight: true,
  }).mount();
});
