document.addEventListener("DOMContentLoaded", function () {
  const hamburger = document.querySelector(".main-nav-hamburger");
  hamburger.addEventListener("click", menuToggle);

  function menuToggle() {
    const items = document.querySelectorAll(".main-nav-item");
    items.forEach((item, idx) => {
      if (idx === 0) return;
      item.classList.toggle("show-item");
    });
    hamburger.classList.toggle("clicked");
  }
});
